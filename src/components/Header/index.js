import Link from "next/link";
import Image from "next/image";

const Header = () => {
  return (
    <nav className="bg-white p-4">
      <div className="flex items-center justify-between">
        <div className="flex items-center">
          <Image src="/logo.png" alt="Logo" width={178} height={52} />
        </div>

        <div className="flex items-start">
          <div className="mr-4">
            <div className="mb-2">
              <Link href="#">
                <span className="cursor-pointer text-gray-700 text-sm leading-5 px-4">
                  Solutions
                </span>
              </Link>
            </div>
            <div className="mb-2">
              <Link href="#">
                <span className="cursor-pointer text-gray-700 text-sm leading-5 px-4">
                  Locations
                </span>
              </Link>
            </div>
          </div>
          <div className="mr-4">
            <div className="mb-2">
              <Link href="#">
                <span className="cursor-pointer text-gray-700 text-sm leading-5 px-4">
                  Contact Us
                </span>
              </Link>
            </div>
            <div className="mb-2">
              <Link href="#">
                <span className="cursor-pointer text-gray-700 text-sm leading-5 px-4">
                  Entreprise
                </span>
              </Link>
            </div>
          </div>
          <div>
            <Link href="#">
              <span className="cursor-pointer text-gray-700 text-sm leading-5 font-semibold uppercase px-4">
                Log in
              </span>
            </Link>
            <Link href="#">
              <span className="relative ml-4 cursor-pointer text-gray-700 text-sm leading-5 font-semibold uppercase px-4">
                <span className="absolute top-1 left-0 w-3 h-3 bg-black rounded-full" />
                Book a visite
              </span>
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Header;
