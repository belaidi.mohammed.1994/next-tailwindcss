import Link from "next/link";
import Image from "next/image";

const HeroHeader = () => {
  return (
    <header className="flex items-center justify-between">
      <div>
        <h2 className="text-4xl font-bold font-archia text-gray-700 uppercase leading-70 text-left mb-4">
          198 Avenue Liberte
        </h2>
        <div className="flex items-center">
          <span className="text-sm font-normal font-bodyS text-gray-600">
            <img
              className="w-4 h-4 inline-block mr-2"
              src="/icon/location.png"
              alt="Location Icon"
            />
            1875 K St NW Washington, DC 20006
          </span>
          <div className="border-l mx-2 h-4"></div>
          <span className="text-sm font-normal font-bodyS text-gray-600">
            <img
              className="w-4 h-4 inline-block mr-2"
              src="/icon/person.png"
              alt="Location Icon"
            />
            2 Guests
          </span>
        </div>
      </div>

      <div className="flex space-x-4">
        <button className="bg-gray-300 rounded-full h-12 w-12 flex items-center justify-center">
          <img src="/icon/icon01.png" alt="Icon 1" className="h-4 w-4" />
        </button>
        <button className="bg-gray-300 rounded-full h-12 w-12 flex items-center justify-center">
          <img src="/icon/icon02.png" alt="Icon 1" className="h-4 w-4" />
        </button>
      </div>
    </header>
  );
};

export default HeroHeader;
