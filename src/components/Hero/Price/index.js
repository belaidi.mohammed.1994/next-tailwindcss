export default function PriceCard() {
  return (
    <div>
      <div className="bg-gray-100 mx-auto max-w-376 p-6">
        <span className="font-bold text-2xl leading-9 tracking-tight text-left">
          127${" "}
        </span>
        <span className="font-normal text-base leading-6 tracking-normal text-left">
          / hour
        </span>
        <p className="mt-4">Some text here...</p>
      </div>
    </div>
  );
}
