export default function ImageSection() {
  return (
      <div className="grid grid-cols-4 gap-4">
        <div className="">
          <div>
            <img className="h-1/3" src="/images/image1.png" />
          </div>
          <div>
            <img className="h-1/3" src="/images/image1.png" />
          </div>
          <div>
            <img className="h-1/3" src="/images/image1.png" />
          </div>
        </div>
        <div className="col-span-3">
          <img className="w-751 h-475.16" src="/images/image1.png" />
        </div>
      </div>
  );
}
