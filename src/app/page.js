// Components
import Header from "../components/Header";
import HeroHeader from "../components/Hero/Header";
import PriceCard from "../components/Hero/Price";
import ImageSection from "../components/Hero/ImageSection";

export default function Home() {
  return (
    <div className="p-4 min-h-screen bg-white">
      <Header />
      <div className="p-4 mx-auto mt-10">
        <HeroHeader />
        <div className="flex text-gray-700 justify-between items-start py-16">
          <div className="w-2/3">
            <div>
              <ImageSection />
            </div>
          </div>
          <div className="w-1/3 sticky top-0">
            <PriceCard />
          </div>
        </div>
      </div>
    </div>
  );
}
